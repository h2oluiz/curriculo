Luiz Carlos dos Santos
==========

Contato:
h2oluiz@gmail.com


###Experiência:

---

**SIGEF - Sistema de Gestão Fundiária** Brasília-DF

Sistema desenvolvido pelo INCRA/MDA para gestão de informações fundiárias do meio rural brasileiro.

Por ele são efetuadas a recepção, validação, organização, regularização e disponibilização das informações georreferenciadas de limites de imóveis rurais. 

O software tem como plataforma:

* Servidor UBUNTO SERVER e CENTOS 6.5;
* Módulos desenvolvidos em python 2.6;
* Framework Django 1.4.5;
* Banco de dados: PostgreSQL;


**Regine de contrato:** CLT

**MicroAgreste (Correspondente MASTER CEF Nordeste)** Recife-PE

Sistema de correspondência bancaria Crediário Caixa Fácil, software que gerencia a
concessão de microcrédito para “linha branca” junto a lojista de Pernambuco
com atuação em todas agências Caixa e lojistas credenciados em todo estado
com foco na terceirização do serviço bancário.

Atuei principalmente na adaptação do modelo de negócio proposto pela
CAIXA(Matriz) para realidade local na forma de sistema.
Detalhes aqui http://www.caixa.gov.br/voce/crediario_caixa_facil/ 

O software tem como plataforma:

* Servidor Debian, hospedado cloud Mandic, Apache2 e WSGI;
* Módulos desenvolvidos em python 2.6;
* Framework Django 1.5;
* Banco de dados: Mysql;

**Regine de contrato:** Prestador de serviço

---

**Franquia People- Formação Completa**

Instrutor de aprendizagem em Informática Ministrando aulas no curso de Lógica de Programação, Banco de Dados,
hardware, montagem de rede e servidores.

**Regine de contrato:** Prestador de serviço

---

**Novo Aviamento LTDA** Caruaru-PE

Empresa atua na venda de varejo e atacado de aviamentos e necessitava de uma solução de sincronização de banco de dados entre as filias.

**Regine de contrato:** Prestador de serviço

---

###Formação
**Universidade do Sul de Santa Catarina (Unisul)**
* Tecnólogo Web e Programação (Sistema para Internet)
* Situação: Cursando
 
**Faculdade de Filosofia Ciências e Letras de Caruaru (FAFICA)**

* Tecnólogo Sistema de Informação
* Situação: Trancado

####Curso Livre:

* Linux - SENAC CARUARU
* Administração de redes Linux I e II - FABEJA
* Manutenção de computadores
* Computação Gráfica para WEB
* Curso de extensão em Programação PHP
* Configuração básica de roteadores CISCO
* Treinamento Franquia People – Desenvolvimento de jogos - RECIFE
* Computação nas Nuvens: Um Novo Jeito de Fazer Negócios - UPE CARUARU 
* Curso "Como identificar Oportunidades e Criar Seu Próprio Negócio" - SEBRAE
* Formação de Educadores Sociais - PAMEN Triunfu
